#!/bin/python

import sys
import RPi.GPIO as GPIO
import time

SENSOR_PIN=23
LED_PIN=2

check_power = 0
print(str(check_power))

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
GPIO.setup(SENSOR_PIN, GPIO.IN)
GPIO.setup(LED_PIN,GPIO.OUT)

path="/home/pi/Documents/motion_log"

RPWM = GPIO.PWM(LED_PIN,100)
RPWM.start(5)

def dimm():
	for x in range(100,5,-2):
		RPWM.start(x)
		time.sleep(0.1)

def power():
	for x in range(5,100,2):
		RPWM.start(x)
		time.sleep(0.1)

def write_file(str_):
	file=open(path,'w')
	file.write(str_)
	file.close()

def main_callback(channel):

	check_power=1
	start = time.time()
	val = time.time() -start
	RPWM.start(25)
	while(val<=3):
		if(check_power==1):
			print("1")
			write_file("1")
			check_power=0
		power()
		val = time.time()-start

	write_file("0")
	print("0")
	dimm()
try:
	write_file("0")
	GPIO.add_event_detect(SENSOR_PIN,GPIO.BOTH,callback=main_callback)
	while True:
		time.sleep(1)

except KeyboardInterrupt:
        print("Beende...")

GPIO.cleanup()
