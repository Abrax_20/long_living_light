#!/bin/python

import RPi.GPIO as GPIO
import time

SENSOR_PIN=23
LED_PIN=2

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
GPIO.setup(SENSOR_PIN, GPIO.IN)
GPIO.setup(LED_PIN,GPIO.OUT)

path="/home/pi/Documents/log"

RPWM = GPIO.PWM(LED_PIN,100)
RPWM.start(5)

#class definition for lamp
class Lamp:
	status = False

	def setStatus(self,change):
		if self.status != change:
			self.status = change

			if self.status == True:
				print("1")
			else:
				print("0")


def dimm():
	for x in range(100,5,-2):
		RPWM.start(x)
		time.sleep(0.1)

def power():
	for x in range(5,100,2):
		RPWM.start(x)
		time.sleep(0.1)

#Object
lamp = Lamp()

def main_callback(channel):

	start = time.time()
	val = time.time() -start
	RPWM.start(25)
	while(val<=3):
		
		Lamp.setStatus(lamp, True)
		power()	
		val = time.time()-start

	#Lamp.setStatus(lamp,False)
	dimm()
	#GPIO.output(LED_PIN,GPIO.LOW)
try:
	GPIO.add_event_detect(SENSOR_PIN,GPIO.BOTH,callback=main_callback)
	while True:
		Lamp.setStatus(lamp, False)
		time.sleep(0.1)

except KeyboardInterrupt:
        print("Beende...")

GPIO.cleanup()
