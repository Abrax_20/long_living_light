import 'babel-polyfill';
import fetch from 'node-fetch';

import getRSPI, {
  NEW_INPUT_EVENT,
} from './../events/getRSPI';

export const lamp = (socket) => {
  socket.emit(NEW_INPUT_EVENT, lamp);
  getRSPI.on(NEW_INPUT_EVENT, async (lamp) => {
    try {
      socket.emit(NEW_INPUT_EVENT, lamp);
    } catch (err) {
      console.error("Some Error at" + NEW_INPUT_EVENT);
    }
  });
};
export default lamp;
