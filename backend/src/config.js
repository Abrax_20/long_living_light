// @flow
export const PORT = process.env.PORT || 8080;
export const DOMAIN = 'localhost';
export const HOST = process.env.HOST || 'http://192.168.188.27:' + PORT;
export const DATABASE = 'mongodb://mongo/ace';
// export const DATABASE = 'mongodb://localhost/ace';
export const RUNNINGMODE = 'dev';
const config = {
  PORT,
  DOMAIN,
  DATABASE,
  RUNNINGMODE,
};
export default config;
