// @flow
import './client';
import express from 'express';
import bodyParser from 'body-parser';
import morgan from 'morgan';
import mongoose from 'mongoose';

import {
  PORT,
  DOMAIN,
  DATABASE,
  RUNNINGMODE
} from './config';



import routes from './routes';

let app = express();
// var io = require('socket.io')(http);

mongoose.connect(DATABASE, { useMongoClient: true });
mongoose.connection.on('error', (err) => console.error('connection error:', err));
mongoose.Promise = global.Promise;


app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(morgan(RUNNINGMODE));

// Weclome msg
app.get('/api', (req, res) => {
  res.json({ msg: 'Welcome to the coolest API on earth!' });
});

// Setup Routes
app.use('/api', routes);
app.use((req, res) => {
  res.status(404).json({ msg: "no route provided" });
});


//SocketIO
// io.on('connection', (socket) => {
//   console.log('a user connected');
// })
//
// http.listen(PORT);

app.listen(PORT);

console.log('Magic happens at http://' + DOMAIN + ':' + PORT);

export default app;
