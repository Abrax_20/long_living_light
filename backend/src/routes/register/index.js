// @flow
import express from 'express';
import register from './post/register';

let routes = express.Router();

routes.post('/', register);

export default routes;
