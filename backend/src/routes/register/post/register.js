// @flow
import 'babel-polyfill';
import {
  string as checkString,
  number as checkNumber,
    boolean as checkBoolean,
} from './../../../validations/utility';
import { bodyValue as checkBodyValue } from './../../../validations/request';
import { Lamp } from './../../../models';

const register = async (req: Object, res: Object) => {
  console.log(req.body);
  try {
    console.log(req.body);
    let lat = checkBodyValue('lat', req, checkNumber);
    let lng = checkBodyValue('lng', req, checkNumber);
    let temp = checkBodyValue('temp', req, checkNumber);
    let luftf = checkBodyValue('luft', req, checkNumber);
    let status = checkBodyValue('status', req, checkBoolean);
    console.log(req.body);

    if (lat == null) {
      res.status(400).json({ error: true, msg: 'bad request' });
      return;
    }

    if (lng == null) {
      res.status(400).json({ error: true, msg: 'bad request' });
      return;
    }

      if (temp == null) {
          res.status(400).json({ error: true, msg: 'bad request' });
          return;
      }

      if (luftf == null) {
          console.log('luftf');
          res.status(400).json({ error: true, msg: 'bad request' });
          return;
      }
    const adress = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    const lamp = new Lamp({ lng, lat, adress, temp, luftf, status });
    await lamp.save();
    res.json({ id: lamp._id });
  } catch (err){
    res.status(500).json({ error: true, msg: 'Internal Server Error' });
  }
}

export default register;
