// @flow
import 'babel-polyfill';
import { Lamp } from './../../../models';

const getAllLamps = async (req: Object, res: Object) => {
  try {
    let lamps = await Lamp.find();

    if (!lamps) {
      res.status(400).json({ error: true, msg: 'bad request' });
      return;
    }

    res.json(lamps);
  } catch (err){
    res.status(500).json({ error: true, msg: 'Internal Server Error' });
  }
}

export default getAllLamps;
