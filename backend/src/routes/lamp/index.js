// @flow
import express from 'express';
import getLamp from './post/getLamp';
import newAdress from './post/newAdress';
import setLampOn from './post/setLampOn';
import setLampOff from './post/setLampOff';
import getAllLamps from './get/getAllLamps';

let routes = express.Router();

routes.post('/', getLamp);
routes.get('/', getAllLamps);
routes.post('/on', setLampOn);
routes.post('/me', newAdress);
routes.post('/off', setLampOff);

export default routes;
