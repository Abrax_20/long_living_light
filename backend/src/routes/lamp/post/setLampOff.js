// @flow
import 'babel-polyfill';
import {
  objectId as checkObjectId,
} from './../../../validations/utility';
import { bodyValue as checkBodyValue } from './../../../validations/request';
import { Lamp } from './../../../models';
import setRSPI from '../../../events/setRSPI';

const setLampOn = async (req: Object, res: Object) => {
  try {
    let id = checkBodyValue('id', req, checkObjectId);

    if (id === null) {
      res.status(400).json({ error: true, msg: 'bad request' });
      return;
    }

    let lamp = await Lamp.findOne({ _id: id });

    if (!lamp) {
      res.status(400).json({ error: true, msg: 'bad request' });
      return;
    }

    setRSPI.setLampOff(lamp);
    lamp.status = false;
    lamp.save();

    res.json({ success: true })
  } catch (err){
    console.log(err);
    res.status(500).json({ error: true, msg: 'Internal Server Error' });
  }
}

export default setLampOn;
