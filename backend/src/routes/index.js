// @flow
import express from 'express';

import send from './send';
import lamp from './lamp';
import event from './event';
import register from './register';

let routes = express.Router();

routes.use('/register', register);
routes.use('/event', event);
routes.use('/lamp', lamp);
routes.use('/send', send);

export default routes;
