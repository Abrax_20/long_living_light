// @flow
import express from 'express';
import onEvent from './post/onEvent';
import statusLamp from './post/statusLamp';

let routes = express.Router();

routes.post('/', onEvent);
routes.post('/status', statusLamp);

export default routes;
