// @flow
import 'babel-polyfill';
import {
  object as checkObject,
  objectId as checkObjectId,
} from './../../../validations/utility';
import { bodyValue as checkBodyValue } from './../../../validations/request';
import { Lamp } from './../../../models';

const onEvent = async (req: Object, res: Object) => {
  try {
    let id = checkBodyValue('id', req, checkObjectId);
    let event = checkBodyValue('event', req, checkObject);

    if (id === null) {
      res.status(400).json({ error: true, msg: 'bad request' });
      return;
    }

    if (event == null) {
      res.status(400).json({ error: true, msg: 'bad request' });
      return;
    }

    let lamp = await Lamp.findOne({ _id: id });

    if (!lamp) {
      res.status(400).json({ error: true, msg: 'bad request' });
      return;
    }

    console.log(lamp.events);
    lamp.events.push({ hallo: 1 });
    console.log(lamp.events);
    await lamp.save();
    res.json({ success: true });
  } catch (err){
    console.log(err);
    res.status(500).json({ error: true, msg: 'Internal Server Error' });
  }
}

export default onEvent;
