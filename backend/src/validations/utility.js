// @flow
import { htmlEncode } from 'htmlencode';
import mongoose from 'mongoose';

export const boolean = (value: any): Boolean | null => {
  if (typeof value !== 'boolean') {
    return null;
  }

  return value;
}

export const number = (value: any): number | null => {
  if (typeof value !== 'number') {
    let number = parseInt(value);
    if (typeof number !== 'number') {
      return null;
    }
    return number;
  }

  return value;
}

export const string = (value: any): String | null => {
  if (typeof value !== 'string') {
    return null;
  }

  return value;
}


export const object = (value: any): Object | null => {
  if (typeof value !== 'object') {
    return null;
  }

  return value;
}

export const array = (value: any): array | null => {
  if (value instanceof Array
      || Array.isArray(value)
      || (value && value !== Object.prototype && array(value.__proto__))
     ) {
       return value;
    }

    return null;
}

export const objectId = (value: any): String | null => {
  if (string(value) === null ||
      !mongoose.Types.ObjectId.isValid(value)) {
    return null;
  }

  return value;
}

export const objectValue = (
  value: any,
  object: Object,
  checker: Function = (value: any): any => value
) => {
  let requestValue = null;

  if (object && value instanceof Array) {
    let objectValue = {}
    for(let x=0;value.length > x;x++) {
      if (x == 0) {
        if (!object[value[x]]) {
          return null;
        }
        objectValue = object[value[x]];
      } else {
        if (!objectValue[value[x]]) {
          return null;
        }

        objectValue = objectValue[value[x]]
      }
    }

    requestValue = checker(objectValue);
  } else {
    if (!object && !object[value]) {
      return null;
    }

    requestValue = checker(object[value]);
  }

  if (typeof requestValue === 'string') {
    requestValue = htmlEncode(requestValue);
  }

  return requestValue;
}
