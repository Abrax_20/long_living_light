// @flow
import { htmlEncode } from 'htmlencode';

export const bodyValue = (
  value: any,
  req: Object,
  checker: Function = (value: any): any => value
) => {
  let requestValue = null;

  if (req.body && value instanceof Array) {
    let objectValue = {}
    for(let x=0;value.length > x;x++) {
      if (x == 0) {
        if (!req.body[value[x]]) {
          return null;
        }
        objectValue = req.body[value[x]];
      } else {
        if (!objectValue[value[x]]) {
          return null;
        }

        objectValue = objectValue[value[x]]
      }
    }

    requestValue = checker(objectValue);
  } else {
    if (!req.body && !req.body[value]) {
      return null;
    }

    requestValue = checker(req.body[value]);
  }

  if (typeof requestValue === 'string') {
    requestValue = htmlEncode(requestValue);
  }

   return requestValue;
}

export const paramsValue = (
  value: string,
  req: Object,
  checker: any = (value: any): any => value
) => {
  let requestValue = null;

  if (!req.params && !req.params[value]) {
    return null;
  }

  requestValue = checker(req.params[value]);

  if (typeof requestValue === 'string') {
    requestValue = htmlEncode(requestValue);
  }

   return requestValue;
}

export const queryValue = (
  value: string,
  req: Object,
  checker: any = (value: any): any => value
) => {
  let requestValue = null;

  if (!req.query && !req.query[value]) {
    return null;
  }

  requestValue = checker(req.query[value]);

  if (typeof requestValue === 'string') {
    requestValue = htmlEncode(requestValue);
  }

   return requestValue;
}
