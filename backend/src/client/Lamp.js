import 'babel-polyfill';
import fetch from 'node-fetch';

import setRSPI, {
  SET_LAMP_ON_EVENT,
  SET_LAMP_OFF_EVENT
} from './../events/setRSPI';

setRSPI.on(SET_LAMP_ON_EVENT, async (lamp) => {
  try {
    let response = await fetch('http://' + lamp.adress + '/');
    response = await response.json();
    console.log(response);
  } catch (err) {
    console.error("Some Error at" + SET_LAMP_ON_EVENT);
  }
});

setRSPI.on(SET_LAMP_OFF_EVENT, async (lamp) => {
  try {
    let response = await fetch('http://' + lamp.adress + '/');
    response = await response.json();
    console.log(response);
  } catch (err) {
    console.error("Some Error at" + SET_LAMP_OFF_EVENT);
  }
});
