// @flow
import EventEmitter from 'events';

export const NEW_INPUT_EVENT = 'NEW_INPUT_EVENT';

export class GetRSPI extends EventEmitter {
  newInputEvent(lamp: Object, event: Object) {
    this.emit(NEW_INPUT_EVENT, lamp, event);
  }
}
export const getRSPI = new GetRSPI();
export default getRSPI;
