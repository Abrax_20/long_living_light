// @flow
import EventEmitter from 'events';

export const SET_LAMP_ON_EVENT = 'SET_LAMP_ON_EVENT';
export const SET_LAMP_OFF_EVENT = 'SET_LAMP_OFF_EVENT';

export class SetRSPI extends EventEmitter {
  setLampOn(lamp: Object) {
    // this.emit(SET_LAMP_ON_EVENT, lamp);
  }

  setLampOff(lamp: Object) {
    // this.emit(SET_LAMP_OFF_EVENT, lamp);
  }
}

export const setRSPI = new SetRSPI();
export default setRSPI;
