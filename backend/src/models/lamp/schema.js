// @flow
import {Schema} from 'mongoose';

let LampSchema = new Schema({
    adress: {
        type: String,
        required: true,
    },
    status: {
        type: Boolean,
        default: false,
    },
    lat: {
        type: Number,
        required: true,
    },
    lng: {
        type: Number,
        required: true,
    },
    temp: {
        type: Number,
        required: false
    },
    luftf: {
        type: Number,
        required: false
    },
    events: {
        type: [],
        required: false,
        default: []
    },
}, {timestamps: true, usePushEach: true});

export default LampSchema;
