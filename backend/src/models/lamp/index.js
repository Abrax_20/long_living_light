// @flow
import mongoose from 'mongoose';
import LampSchema from './schema';

const Lamp = mongoose.model('lamps', LampSchema);
export default Lamp;
