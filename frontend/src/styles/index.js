import media from './media';

export const colors = {
  //main: '#8EF478',
  main: '#009900',
  green: '#DBF48C',
  pink: '#de256e',
  grey: '#AAAAAA',
  white: '#FFFFFF',
  black: '#000000',
  secound: '#0C4270',
  darkGrey: '#778899',
  lightGrey: '#dcdcdc',
  lightWhite: '#EFEFEF',
  blue: '#0066cc',
};
export const font = {
  h1: `
    font-size: 48px;
    font-family: 'PT Sans', sans-serif;
  `,
  h2: `
    font-size: 32px;
    font-family: 'PT Sans', sans-serif;
  `,
  h3: ``,
  h4: `
    font-size: 24px;
    font-family: 'PT Sans', sans-serif;
  `,
  h5: ``,
  h6: ``,
  p: ``,
  default: `
    font-size: 16px;
    font-family: 'PT Sans', sans-serif;
  `
};
export const size = {
  borderWidth: [
    '0.5px',
    '1px',
    '1.5px',
    '2px',
  ],
  padding: [
    //padding@1
    '4px',
    //padding@2
    '8px',
    //padding@3
    '16px',
    //padding@4
    '32px',
    //padding@5
    '48px',
    //padding@6
    '64px',
    //padding@7
    '80px',
    //padding@8
    '96px',
    //padding@9
    '112px',
    //padding@10
    '128px',
    //padding@11
    '144px',
    //padding@12
    '160px',
  ],
  components: {
    Buttons: {
      MainButton: {
        height: '40px',
        borderRadius: '20px',
      }
    },
    card: {
      width: '700px'
    },
    Header: {
      Logo: {
        height: '40px',
      }
    }
  },
  card: {
    width: '700px'
  }
};
export {
  media,
};
