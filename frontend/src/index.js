import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { SOCKET_URI } from './constants/api';
import initalSocketHandler from './socket';
import { ConnectedRouter } from 'react-router-redux';
import store, { history } from './store';
import App from './routes';

//const socket = openSocket(SOCKET_URI);
// initalSocketHandler(socket);

const target = document.querySelector('#root')

fetch("http://192.168.188.27:5002/api").then((stuff) => console.log(stuff)).catch((err) => console.log(err));

render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <App />
    </ConnectedRouter>
  </Provider>,
  target
)
