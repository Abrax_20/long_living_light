// @flow
import React, { Component } from 'react';
import { Route } from 'react-router-dom';

import Wrapper from './components/Wrapper';

// Screens
import Dashboard from './screens/Dashboard';
import LampMap from  './screens/LampMap';

class App extends Component<{}> {
  render() {
    return (
      <Wrapper>
        <Route exact path="/" component={Dashboard} />
        <Route exact path="/dashboard" component={Dashboard} />
        <Route exact path="/maps" component={LampMap} />
      </Wrapper>
    );
  }
}

// Authenticated
export default App;
