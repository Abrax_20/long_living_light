import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import lamps from './lamps';

export default combineReducers({
  lamps,
  router: routerReducer,
})
