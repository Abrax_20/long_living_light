import { API_URI } from './../constants/api';
import {
  START_FETCH,
  ERROR_FETCH,
  SUCCESS_FETCH,
} from './../actions/api';

export let fetchId = 0;

export const fetchMiddleware = store => next => action => {
  next(action);
  switch (action.type) {
    case START_FETCH: {
      // Setup Options
      let options = {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        ...(action.payload.options ? action.payload.options : {}),
      };
      if (action.payload.options && action.payload.options.body) {
        options['body'] = JSON.stringify(action.payload.options.body);
      }


      fetchId++;
      if (action.payload.callback) {
        action.payload.callback(fetchId);
      }

      // Start Fetching
      fetch(API_URI + action.payload.path, options)
        .then(response => {
          if (response.status === 200) {
            return response;
          }

          //eslint-disable-next-line no-throw-literal
          throw { fetchId, type: action.payload.type }
        })
        .then(response => response.json())
        .then((data) => {
          store.dispatch({
            type: SUCCESS_FETCH,
            payload: {
              data,
              fetchId,
              type: action.payload.type,
              body: action.payload.options && action.payload.options.body
                ? action.payload.options.body
                : {},
            }
          });
        })
        .catch(error => store.dispatch({
          type: ERROR_FETCH,
          payload: {
            type: action.payload.type,
            error
          }
        }));
      break;
    }
    default:
      break;
  }
};
export default fetchMiddleware;
