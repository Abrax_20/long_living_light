// @flow
import React, {Component} from 'react'
// React Componets
import Menu from './../../components/Menu';
import Card from './../../components/Card';
// Styled Components
import Content from './styled/Content';
import Container from './styled/Container';
import Button from "@material-ui/core/es/Button/Button";
import CardActions from "@material-ui/core/es/CardActions/CardActions";
import Collapse from "@material-ui/core/es/Collapse/Collapse";
import CardContent from "@material-ui/core/es/CardContent/CardContent";
import classnames from 'classnames';
import Diagramm1 from "./../../components/Diagramm1";
import Diagramm2 from "./../../components/Diagramm2";
import Diagramm3 from "./../../components/Diagramm3";
import Diagramm4 from "./../../components/Diagramm4";

type DashboardType = {};

export class Dashboard extends Component<DashboardType> {
    componentDidMount() {
        this.props.getLamps();
    }


    state = { diagramm: false };

    handleExpandClick = (number) => {
      console.log(number);
      if (number == this.state.diagramm) {
        this.setState({ diagramm: 0 });
      } else {
        this.setState({ diagramm: number })
      }
    };

    render() {
      console.log(this.state);
        const { classes } = this.props;
        return (
            <Container>
                <Menu/>
                <Content>
                    <Card
                      actions={(
                        <CardActions style={{flexDirection: 'column'}}>
                          <Button onClick={() => this.handleExpandClick(1)} aria-label="Show">More</Button>
                        </CardActions>
                      )}
                      title={'16°C'}
                      text={'Temperatur'} />
                    <Card
                      actions={(
                        <CardActions style={{flexDirection: 'column'}}>
                          <Button onClick={() => this.handleExpandClick(2)} aria-label="Show">More</Button>
                        </CardActions>
                      )}
                      title={'64%'}
                      text={'Luftfeuchtigkeit'} />
                    <Card
                      actions={(
                        <CardActions style={{flexDirection: 'column'}}>
                          <Button onClick={() => this.handleExpandClick(3)} aria-label="Show">More</Button>
                        </CardActions>
                      )}
                      title={'14 km/h'}
                      text={'Wind'} />
                    <Card
                      actions={(
                        <CardActions style={{flexDirection: 'column'}}>
                          <Button onClick={() => this.handleExpandClick(4)} aria-label="Show">More</Button>
                        </CardActions>
                      )}
                      title={'3,5 µg/m³'}
                      text={'Ozon'} />
                    <div style={{ width: '100%' }}></div>
                    { this.state.diagramm == 1 ? (<Diagramm1/>) : null }
                    { this.state.diagramm == 2 ? (<Diagramm2/>) : null }
                    { this.state.diagramm == 3 ? (<Diagramm3/>) : null }
                    { this.state.diagramm == 4 ? (<Diagramm4/>) : null }
                </Content>
            </Container>
        );
    }
}

Dashboard.defaultProps = {};

export default Dashboard;
