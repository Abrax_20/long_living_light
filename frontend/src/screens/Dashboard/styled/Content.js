import styled from 'styled-components';
import { size } from './../../../styles';

export const Content = styled.div`
  flex: 1;
  display: flex;
  flex-wrap: wrap;
  overflow-: hidden;
  overflow-y: scroll;
  align-items: flex-start;
  justify-content: flex-start;
  padding: ${size.padding[3]};
  &::after {
    content: '';
    width: 100%;
  }
  &:last-child {
    order: 1;
  }
`;
export default Content;
