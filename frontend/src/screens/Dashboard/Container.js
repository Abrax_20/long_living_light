// @flow
import { connect } from 'react-redux';
import Dashboard from './Dashboard';
import { getLamps } from './../../actions/api';

export const mapStateToProps = (state: Object) => ({});
export const mapDispatchToProps = (dispatch: Function) => ({
  getLamps: () => dispatch(getLamps()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
