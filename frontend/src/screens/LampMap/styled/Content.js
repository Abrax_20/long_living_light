import styled from 'styled-components';

export const Content = styled.div`
  flex: 6;
`;
export default Content;
