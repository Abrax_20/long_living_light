// @flow
import React, {Component} from 'react';

// React Components

import LampInfo from './../../components/LampInfo';
import Menu from './../../components/Menu';
import GoogleLampMap from './../../components/LampMap';


// Styled Components
import Content from './styled/Content';
import Container from './styled/Container';

type LampMapPropsType = {
    lamps: Array<Object>,
    setStatus: Function
};

export class LampMap extends Component<LampMapPropsType> {
    constructor(props) {
      super(props);
      this.state = {
        lamp: null,
        menu: false,
      };
    }

    componentDidMount() {
      this.props.getLamps();
      setInterval(() => this.props.getLamps(), 3000);
    }

    toggle(lamp) {
      this.setState({ menu: !this.state.menu, lampId: lamp._id })
    }

    render() {
      const lamp = this.props.lamps.filter((lamp) => this.state.lampId == lamp._id)[0];
      return (
          <Container>
              <Menu/>
              <Content>
                <GoogleLampMap
                    onMarkerClick={(lamp) => this.toggle(lamp)}
                    markers={this.props.lamps}
                    containerElement={<div id={'toolTest'} style={{height: `100%`}}/>}
                    mapElement={<div style={{backgroundcolor: '#ffffcc'},{height: `100%`}}/>}>
                </GoogleLampMap>
              </Content>
              {
                this.state.menu
                  ? <LampInfo setStatus={this.props.setStatus} lamp={lamp} />
                  : null
              }
          </Container>
        );
    }
}

LampMap.defaultProps = {
    lamps: [],
    setStatus: () => {}
};

export default LampMap;
