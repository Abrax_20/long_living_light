// @flow
import { connect } from 'react-redux';
import LampMap from './LampMap';
import { getLamps, setStatus } from './../../actions/api';

export const mapStateToProps = (state: Object) => ({
  lamps: state.lamps
});
export const mapDispatchToProps = (dispatch: Function) => ({
  getLamps: () => dispatch(getLamps()),
  setStatus: (id, status) => dispatch(setStatus(id, status)),
});

export default connect(mapStateToProps, mapDispatchToProps)(LampMap);
