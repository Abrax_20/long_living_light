// @flow
import React, { Component } from 'react';
import styled, { injectGlobal } from "styled-components";

export type WrapperType = {
  children: any
};


injectGlobal`
  body {
    padding: 0;
    margin: 0;
    height: 100%;
    font-size: 5vw;
  }

  html, #root, #root>div {
    height: 100% !important;
  }

  a {
    color: inherit;
    cursor: pointer;
    text-decoration: inherit;
  }

  @media only screen and (min-width: 480px) {
    body {
      font-size: 23px;
    }
  }
`;

const Container = styled.main`
  height: 100%;
  display: flex;
  position: relative;
`;

export default class Wrapper extends Component<WrapperType> {
  render() {
    return (
      <Container>
        { this.props.children }
      </Container>
    );
  }
}
