import styled, { keyframes } from 'styled-components';

const open = keyframes`
  from { width: 0px; }
  to { width: 30vw; }
`;

export const Container = styled.div`
  animation-name: ${open};
  animation-duration: 400ms;
  width: 30vw;
  overflow-x: hidden;
  overflow-y: scroll;
`;
export default Container;
