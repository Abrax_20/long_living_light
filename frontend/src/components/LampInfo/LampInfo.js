// @flow
import React, {Component} from 'react';

// Styled Components
import Container from './styled/Container';
import LampMap from "../LampMap/LampMap";
import Card from "../Card/Card";
import Button from "@material-ui/core/es/Button/Button";
import CardActions from "@material-ui/core/es/CardActions/CardActions";

type LampInfoPropsType = {};

export default class LampInfo extends Component<LampInfoPropsType> {

    tempChange(temp) {

        var fahrenheit = 9 / 5 * temp + 32;

        return fahrenheit;
    }

    activity(status) {
        console.log('status');

        var color = '';

        if (status === true) {
            color = '#33cc33';
        } else {
           color = '#ff3300';
        }
        console.log(color);
        return color;
    }

    render() {
      console.log(this.props.lamp);
        return (
            <Container>
                <Card title={'Latitude'} text={this.props.lamp.lng}/>
                <Card title={'Longitude'} text={this.props.lamp.lat}/>
                <Card title={'Temperature'}
                      text={this.props.lamp.temp + '°C' + ' (' + this.tempChange(this.props.lamp.temp) + 'F)'}/>
                {/* <Card title={'Humidity'} text={th}/> */}
                <Card
                  title={this.props.lamp.status ? 'on' : 'off'}
                  actions={
                    <CardActions style={{ justifyContent: 'center', alignItems: 'center' }}>
                      <Button onClick={() => this.props.setStatus(this.props.lamp._id, !this.props.lamp.status)}>
                        Toggle
                      </Button>
                    </CardActions>}>
                </Card>
            </Container>
        );
    }
}
