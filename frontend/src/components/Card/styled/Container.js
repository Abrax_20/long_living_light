import styled from 'styled-components';

export const getContainer = (props) => styled.div`
  flex: ${props.flex};
  margin: 17px;
`;
export default getContainer;
