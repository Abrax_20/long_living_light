// @flow
import React, {Component} from 'react';

// React Components
import CardContainer from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';


// Styled Components
import getContainer from './styled/Container';

type CardPropsType = {
    text: ?String,
    flex: ?number,
    style: ?Object,
    title: ?String,
    media: ?Function,
    actions: ?Function,
    children: ?Function,
};

export default class Card extends Component<CardPropsType> {

    render() {
        const Container = getContainer(this.props);
        return (
            <Container flex={this.props.number} style={this.props.style}>
                <CardContainer>
                    {
                        this.props.media ||
                        this.props.children
                            ? (
                                <CardMedia title={this.props.mediaTitle}>
                                    {
                                        this.props.children
                                            ? this.props.children
                                            : this.props.media
                                    }
                                </CardMedia>
                            )
                            : null
                    }
                    <CardContent>
                        <Typography gutterBottom variant="headline" align={"center"} component="h2">
                            {this.props.title}
                        </Typography>
                        <Typography component="p" align={"center"}>
                            {this.props.text}
                        </Typography>
                    </CardContent>
                    {
                        this.props.title
                            ? this.props.actions
                            : null
                    }
                </CardContainer>
            </Container>
        );
    }
}
Card.defaultProps = {
    flex: 1
};
