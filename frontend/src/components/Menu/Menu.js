// @flow
import { Link } from 'react-router-dom';
import React, { Component } from 'react';
import * as FontAwesome from 'react-icons/lib/fa';

// Styled Components
import Item from './styled/Item';
import Container from './styled/Container';

type MenuPropsType = {};

export default class Menu extends Component<MenuPropsType> {
  render() {
    return (
      <Container>
        <Item>
          <Link to={{pathname: '/'}}>
            <FontAwesome.FaHome />
          </Link>
        </Item>
        <Item>
          <Link to={{pathname: '/maps'}}>
            <FontAwesome.FaMap />
          </Link>
        </Item>
      </Container>
    );
  }
}
