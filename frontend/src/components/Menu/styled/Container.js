import styled from 'styled-components';
import { colors, size } from './../../../styles';

export const Container = styled.div`
  flex: 1;
  display: flex;
  max-width: 80px;
  align-items: center;
  flex-direction: column;
  border-right-width: 1px;
  border-right-style: solid;
  border-right-color: ${colors.grey};
  background-color: ${colors.main};
  padding-top: ${size.padding[1]};
  padding-bottom: ${size.padding[1]};
`;
export default Container;
