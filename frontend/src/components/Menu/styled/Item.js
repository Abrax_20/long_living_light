import styled from 'styled-components';
import { colors, font, size } from './../../../styles';

export const Item = styled.p`
  ${font.h2}
  margin: 0px;
  text-align: center;
  color: ${colors.white};
  padding-top: ${size.padding[2]};
  &:hover {
    color: ${colors.secound};
  }
`;
export default Item;
