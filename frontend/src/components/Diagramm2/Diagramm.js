import React, { Component } from 'react';
import { Bar } from 'react-chartjs-2';

const data = {
  labels: ['06.05.', '07.05', '08.05', '09.05.', '10.05.', '11.05', '12.05.'],
  datasets: [
    {
      label: 'My First dataset',
      backgroundColor: 'rgba(0,255,0)',
      borderColor: 'rgba(255,99,132,1)',
      borderWidth: 1,
      hoverBackgroundColor: 'rgba(255,99,132,0.4)',
      hoverBorderColor: 'rgba(255,99,132,1)',
      data: [21, 24, 25, 27, 28, 16, 23, 0],
    }
  ]
};

export default class BarDi extends Component {
  render() {
    return (
      <Bar
        data={data}
        width={250} height={300}
        options={{
          maintainAspectRatio: false
        }}
      />
    );
  }
}
