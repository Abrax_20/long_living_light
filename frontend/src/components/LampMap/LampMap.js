// @flow
import React, { Component } from 'react';
import { Marker, GoogleMap } from 'react-google-maps';
import lamp_on from './../../assets/svg/lamp_on.svg';
import lamp_off from './../../assets/svg/lamp_off.svg';

type LampMapPropsType = {
  markers: ?Array<Object>,
  onMarkerClick: ?Function,
};

export class LampMap extends Component<LampMapPropsType> {
  render() {
    return (
      <GoogleMap defaultZoom={3} defaultCenter={{ lat: 52.4580606, lng: 13.452418 }}>
        {
          this.props.markers.map((marker, key) => (
            <Marker
              key={key}
              onClick={() => this.props.onMarkerClick(marker)}
              icon={marker.status ? lamp_on : lamp_off}
              position={{ lat: marker.lat, lng: marker.lng }} />
          ))
        }
      </GoogleMap>
    );
  }
}
LampMap.defaultProps = {
  markers: [],
  onMarkerClick: () => {},
};

export default LampMap;
