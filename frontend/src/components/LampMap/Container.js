import React from 'react';
import {
  withScriptjs,
  withGoogleMap,
} from 'react-google-maps'
import { compose, withProps } from 'recompose';

import LampMap from './LampMap';

export default compose(
  withProps({
    /**
     * Note: create and replace your own key in the Google console.
     * https://console.developers.google.com/apis/dashboard
     * The key 'AIzaSyBkNaAGLEVq0YLQMi-PYEMabFeREadYe1Q' can be ONLY used in this sandbox (no forked).
     */
    googleMapURL: 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDwOQK8bsgpNoWZuZR0_fWZ1-RL5PHrGAo&v=3.exp&libraries=geometry,drawing,places',
    loadingElement: <div style={{ height: '100vh' }} />,
    containerElement: <div style={{ height: '100vh' }} />,
    mapElement: <div style={{ height: '100%' }} />
  }),
  withScriptjs,
  withGoogleMap
)(props => (<LampMap {...props} isMarkerShown={true}/>));
