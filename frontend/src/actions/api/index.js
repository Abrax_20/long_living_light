import {
  START_FETCH,
  ERROR_FETCH,
  SUCCESS_FETCH,
} from './api';

import {
  getLamps,
  setStatus
} from './lamps';

export {
  START_FETCH,
  ERROR_FETCH,
  SUCCESS_FETCH,

  // Lamps
  getLamps,
  setStatus
};
