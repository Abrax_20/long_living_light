import { START_FETCH } from './api';

export const GET_LAMPS = 'GET_LAMPS';
export const SET_LAMP_STATUS = 'SET_LAMP_STATUS';
export const getLamps = () => ({
  type: START_FETCH,
  payload: {
    type: GET_LAMPS,
    path: '/lamp',
    options: {
      method: 'GET',
    }
  }
});
export const setStatus = (id, status) => ({
  type: START_FETCH,
  payload: {
    type: SET_LAMP_STATUS,
    path: '/lamp' + (status ? '/on' : '/off'),
    options: {
      body: { id },
      method: 'POST',
    }
  }
});
