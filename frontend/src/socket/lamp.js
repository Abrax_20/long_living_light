import store from './../store';

export const LAMP_ON_EVENT = 'LAMP_ON_EVENT';

export const lamp = (socket) => {
  socket.on(LAMP_ON_EVENT, (interval) => {
    store.dispatch();
  })
};
export default lamp;
