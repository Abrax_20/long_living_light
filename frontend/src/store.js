import createHistory from 'history/createBrowserHistory';
import { routerMiddleware } from 'react-router-redux';
import { createStore, applyMiddleware } from 'redux';
import fetch from './middleware/fetch';
import rootReducer from './reducer';
import logger from 'redux-logger';
import thunk from 'redux-thunk';

export const history = createHistory();
const initialState = {};
const middleware = [
  thunk,
  fetch,
  routerMiddleware(history)
];

if (process.env.NODE_ENV === 'development') {
  middleware.push(logger)
}

export const store = createStore(
  rootReducer,
  initialState,
  applyMiddleware(...middleware)
);

export default store;
