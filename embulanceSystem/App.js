/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Text,
  View,
  Button,
  StyleSheet,
} from 'react-native';
import call from 'react-native-phone-call';

type Props = {};
export default class App extends Component<Props> {

  openCall() {
    const args = {
      number: '017632591731', // String value with the number to call
      prompt: false // Optional boolean property. Determines if the user should be prompt prior to the call
    }

    call(args).catch(console.error);
  }

  render() {
    return (
      <View style={styles.container}>
        <Button
          onPress={this.openCall.bind(this)}
          title={"Do you need help?"}
          color={'#14ee14'}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
