import json
import requests

def read ():
    file = open("config.json", "r")
    string = "";
    for line in file:
        string = string + line;
    return string;
def readMotionLog ():
    file = open("motion_log", "r")
    string = "";
    for line in file:
        string = string + line;
    return string;
def readTempLog ():
    file = open("temp_log", "r")
    x = 0;
    temp = 0;
    humidity = 0;
    for line in file:
        if (x == 0):
            temp = line;
        if (x == 1):
            humidity = line;
        x = x + 1;
    return { "temp": temp, "humidity": humidity };
def write (string):
    file = open("config.json", "w")
    file.write(string)

def request(url, data):
    contents = requests.post(url, data=data);
    return contents.text;

def newEvent(event):
    return;
config = json.loads(read());

if len(config["server"]) == 0:
    exit(1)

if len(config["id"]) == 0:
    content = request(
                config["server"] + "/api/register",
                {
                    "lat": 52.458061,
                    "lng": 13.452418,
                    "temp": 0,
                    "luft": 0,
                    "status": False
                });
    id = json.loads(content)["id"];
    config["id"] = id;
    write(json.dumps(config));

temp = { "temp": 0, "humidity": 0 };
motion = 0;

while (True):
    newMotion = readMotionLog();
    newTemp = readTempLog();
    newMotion = newMotion.replace("\n", "")
    newTemp["temp"] = newTemp["temp"].replace("\n", "")
    newTemp["humidity"] = newTemp["humidity"].replace("\n", "")
    if (motion != newMotion):
        motion = newMotion.replace("\n", "");
        newEvent({});
    if (newTemp["temp"] != temp["temp"]):
        temp["temp"] = newTemp["temp"].replace("\n", "");
        newEvent({});
    if (newTemp["humidity"] != temp["humidity"]):
        temp["humidity"] = newTemp["humidity"].replace("\n", "");
        newEvent({});
# request(config["server"] + "/api/lamp", { "lat": 52.458061, "lng": 13.452418 });
